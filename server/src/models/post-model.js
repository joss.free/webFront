const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PostSchema = new Schema({
  timeStamp: {
    type: String,
    //unique: true
  },
  revision: {
    type: Number
  },
  author: {
    type: String
  },
  date: {
    type: String
  },
  action: {
    type: String
  },
  version: {
    type: String
  },
  name: {
    type: String
  },
  state: {
    type: String
  },
  bug: {
    type: String
  },
  pcr: {
    type: String
  }
})

const PostModel = mongoose.model('results', PostSchema)

module.exports = PostModel
