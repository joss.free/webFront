const express = require('express')
const router = express.Router()
const Post = require('../models/post-model')

router.post('/posts', (req, res) => {
  const post = new Post({
    timeStamp: req.body.timeStamp,
    revision: req.body.revision,
    author: req.body.author,
    date: req.body.date,
    action: req.body.action,
    version: req.body.version,
    name: req.body.name,
    state: req.body.state,
    bug: req.body.bug,
    pcr: req.body.pcr
  })
  post.save((err, data) => {
    if (err) {
      console.log(err)
    } else {
      res.send({
        success: true,
        message: `Post with ID_${data._id} saved successfully!`
      })
    }
  })
})

router.get('/posts', (req, res) => {
    console.log("RUN get /posts");
    console.log("URL == " + req.url);
  Post.find({}, 'timeStamp revision author date action version name state bug pcr', (err, posts) => {
    if (err) {
      res.sendStatus(500)
    } else {
      res.send({ posts: posts })
    }
  }).sort({ _id: -1 })
})

router.get('/posts/ko', (req, res) => {
    console.log("RUN get /posts/ko");
    console.log("URL == " + req.url);
  Post.find({state: 'ko'}, 'timeStamp revision author date action version name state bug pcr', (err, posts) => {
    if (err) {
      res.sendStatus(500)
    } else {
      res.send({ posts: posts })
    }
  }).sort({ _id: -1 })
})

router.get('/posts/:id', (req, res) => {
    console.log("RUN get /posts/id");
  Post.findById(req.params.id, 'timeStamp revision author date action version name state bug pcr', (err, post) => {
    if (err) {
      res.sendStatus(500)
    } else {
      res.send(post)
    }
  })
})

router.put('/posts/:id', (req, res) => {
  Post.findById(req.params.id, 'timeStamp revision author date action version name state bug pcr', (err, post) => {
    if (err) {
      console.log(err)
    } else {
        if(req.body.timeStamp){
            post.timeStamp = req.body.timeStamp
        }
        if(req.body.revision){
            post.revision = req.body.revision
        }
        if(req.body.author){
            post.author = req.body.author
        }
        if(req.body.date){
            post.date = req.body.date
        }
        if(req.body.action){
            post.action = req.body.action
        }
        if(req.body.version){
            post.version = req.body.version
        }
        if(req.body.name){
            post.name = req.body.name
        }
        if(req.body.state){
            post.state = req.body.state
        }
        post.bug = req.body.bug
        post.pcr = req.body.pcr
      post.save(err => {
        if (err) {
          res.sendStatus(500)
        } else {
          res.sendStatus(200)
        }
      })
    }
  })
})

router.delete('/posts/:id', (req, res) => {
  Post.remove({ _id: req.params.id }, err => {
    if (err) {
      res.sendStatus(500)
    } else {
      res.sendStatus(200)
    }
  })
})

module.exports = router
